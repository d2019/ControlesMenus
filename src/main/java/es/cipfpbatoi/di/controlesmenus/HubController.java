package es.cipfpbatoi.di.controlesmenus;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class HubController {

    @FXML
    void abrirLogin(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Hub.class.getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 300, 500);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.show();
        System.out.println("Abriendo página de login...");
    }

    @FXML
    void abrirFormulario(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Hub.class.getResource("formulario.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 300, 500);
        stage.setTitle("Formulario");
        stage.setScene(scene);
        stage.show();
        System.out.println("Abriendo formulario...");
    }

    @FXML
    void abrirCalculadora(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Hub.class.getResource("calculadora.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 346, 480);
        stage.setTitle("Calculadora");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
        System.out.println("Abriendo calculadora...");
    }
}
//pantalla2Controller v2pantalla2Controller(pantalla2Controller) fmxloader.getController();
//v2pamtalla2Controller.label2.setText(txtfield.getText())
