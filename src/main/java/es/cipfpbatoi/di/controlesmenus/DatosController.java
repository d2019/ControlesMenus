package es.cipfpbatoi.di.controlesmenus;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class DatosController implements Initializable {

        @FXML
        public Label apellidoLabel;

        @FXML
        public Label ciudadLabel;

        @FXML
        public Label comentarioLabel;

        @FXML
        public Label fechaRealizacion;

        @FXML
        public Label generoLabel;

        @FXML
        public Label horasPc;

        @FXML
        public Label nombreLabel;

        @FXML
        public Label sistemaLabel;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
